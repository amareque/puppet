# Fact: ipv6enabled
#
# Purpose: Check if system has IPv6 enabled.
#
Facter.add(:ipv6enabled) do
  setcode do
    out = Facter::Util::Resolution.exec('ping6 -c 1 ::1 2> /dev/null | egrep " [0\.]*% packet loss"')
    if out
        "true"
    else
        "false"
    end
  end
end

