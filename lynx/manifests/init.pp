
# Install Lynx WWW browser
#
# === Global variables:
#
#   $www_default_url:
#       Default starting URL.
#
class lynx {

    if ! $www_default_url {
        $www_default_url = "http://www.${domain}"
    }

    if $::operatingsystem != "OpenBSD" {
        package { "lynx":
            name   => $::operatingsystem ? {
                "ubuntu"  => "lynx-cur",
                default   => "lynx",
            },
            ensure => installed,
        }
    }

    if $::operatingsystem == "OpenBSD" {
        exec { "add-local-lynx-config":
             command => "echo 'INCLUDE:/etc/lynx-site.cfg' >> /etc/lynx.cfg",
             path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
             user    => root,
             unless  => "fgrep 'INCLUDE:/etc/lynx-site.cfg' /etc/lynx.cfg",
        }
    }

    file { "lynx-site.cfg":
        name    => $::operatingsystem ? {
            "ubuntu" => "/etc/lynx-cur/local.cfg",
            default  => "/etc/lynx-site.cfg",
        },
        ensure  => present,
        content => template("lynx/lynx-site.cfg.erb"),
        mode    => 0644,
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => $::operatingsystem ? {
            "openbsd" => undef,
            default   => Package["lynx"],
        },
    }

}
