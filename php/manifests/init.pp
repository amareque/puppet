
# Install PHP command-line interface
#
class php::cli {

    package { "php-cli":
        ensure => installed,
    }

}


# Install GD support to PHP
#
class php::gd {

    package { "php-gd":
        ensure => installed,
    }

}


# Install MySQL support to PHP
#
class php::mysql {

    package { "php-mysql":
        ensure => installed,
    }

}


# Install PostgreSQL support to PHP
#
class php::pgsql {

    package { "php-pgsql":
        ensure => installed,
    }

}


# Install PDO database abstraction support to PHP
#
class php::pdo {

    package { "php-pdo":
        ensure => installed,
    }

}


# Install DBA suppor to PHP
#
class php::dba {

    package { "php-dba":
        ensure => installed,
    }

}


# Install IMAP support to PHP
#
class php::imap {

    package { "php-imap":
        ensure => installed,
    }

}


# Install LDAP support to PHP
#
class php::ldap {

    package { "php-ldap":
        ensure => installed,
    }

}


# Install Multibyte String support to PHP
#
class php::mbstring {

    package { "php-mbstring":
        ensure => installed,
    }

}


# Install Mcrypt support to PHP
#
class php::mcrypt {

    package { "php-mcrypt":
        ensure => installed,
    }

}


# Install PEAR support to PHP
#
class php::pear {

    package { "php-pear":
        ensure => installed,
    }

}


# Install XML support to PHP
#
class php::xml {

    package { "php-xml":
        ensure => installed,
    }

}
