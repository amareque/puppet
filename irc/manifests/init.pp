# Install IRC server.
#
# === Global variables
#
#   $irc_motd:
#       IRC server message of the day.
#
class irc::server {

    package { "ircd-ratbox":
        ensure => installed,
    }

    file { "/var/lib/ratbox":
        ensure  => directory,
        mode    => "0700",
        owner   => "irc",
        group   => "irc",
        require => Package["ircd-ratbox"],
    }

    file { "/etc/ircd-ratbox/ircd.motd":
        ensure  => present,
        mode    => "0644",
        owner   => "irc",
        group   => "irc",
        content => "${irc_motd}",
        require => Package["ircd-ratbox"],
    }

}


# Install IRC services.
#
class irc::services inherits irc::server {

    package { "ratbox-services-sqlite":
        ensure => installed,
    }

    file { "/var/lib/ratbox-services":
        ensure  => directory,
        owner   => "irc",
        group   => "irc",
        mode    => "0700",
        require => Package["ircd-ratbox", "ratbox-services-sqlite"],
    }

}


# Configure IRC network.
#
# === Parameters
#
#   $name:
#       IRC network name.
#   $desc:
#       IRC network description.
#   $servername:
#       Name of this IRC server.
#   $serverdesc:
#       Desription of this IRC server.
#   $port:
#       Port to listen.
#   $sslport:
#       Port to listen with SSL.
#   $sslcert:
#       Certificate source. Defaults to generated self signed cert.
#   $operator:
#       List of allowed operators. Defaults to ["*@127.0.0.1"].
#   $operpass:
#       Password required for gaining operator privileges.
#   $userpass:
#       Password required for joining this server.
#   $service:
#       Enabled IRC services. Defaults to false.
#   $servpass:
#       Password for IRC services.
#   $ident:
#       Use ident service. Defaults to false.
#
define irc::network($desc, $servername, $serverdesc, $port, $sslport,
                    $sslcert="", $operator=["*@127.0.0.1"], $operpass,
                    $userpass="", $services=false, $servpass="",
                    $ident=false) {

    include irc::server

    if $ident == false {
        $disable_identd = "yes"
    } else {
        $disable_identd = "no"
    }

    file { "/var/lib/ratbox/${name}":
        ensure  => directory,
        owner   => "irc",
        group   => "irc",
        mode    => "0700",
        require => File["/var/lib/ratbox"],
    }

    file { "/var/lib/ratbox/${name}/ircd.conf":
        ensure  => present,
        mode    => "0600",
        owner   => "irc",
        group   => "irc",
        content => $services ? {
            true  => template("irc/ircd-ratbox.conf.erb", "irc/ircd-ratbox-services.conf.erb"),
            false => template("irc/ircd-ratbox.conf.erb"),
        },
        before  => Service["ircd-${name}"],
        notify  => Service["ircd-${name}"],
        require => File["/var/lib/ratbox/${name}"],
    }

    if $sslcert {
        file { "/var/lib/ratbox/${name}/ircd.pem":
            ensure  => present,
            mode    => "0600",
            owner   => "irc",
            group   => "irc",
            source  => "${sslcert}",
            require => File["/var/lib/ratbox/${name}"],
        }
    } else {
        ssl::certificate { "/var/lib/ratbox/${name}/ircd.pem":
            cn      => "${servername}",
            mode    => "0600",
            owner   => "irc",
            group   => "irc",
            require => File["/var/lib/ratbox/${name}"],
        }
    }

    ssl::dhparam { "/var/lib/ratbox/${name}/dh.pem":
        mode    => "0600",
        owner   => "irc",
        group   => "irc",
        require => File["/var/lib/ratbox/${name}"],
    }

    file { "/etc/init.d/ircd-${name}":
        ensure  => present,
        mode    => "0755",
        owner   => root,
        group   => root,
        content => template("irc/ircd-ratbox.init.erb"),
        before  => Service["ircd-${name}"],
        notify  => Exec["enable-ircd-${name}"],
    }

    exec { "enable-ircd-${name}":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "update-rc.d ircd-${name} defaults",
        refreshonly => true,
        before      => Service["ircd-${name}"],
    }

    service { "ircd-${name}":
        ensure  => running,
        enable  => true,
        status  => "pgrep -u irc -f /var/lib/ratbox/${name}",
        restart => "/etc/init.d/ircd-${name} reload",
    }

    if $services == true {
        include irc::services

        if ! $servpass {
            fail("\$servpass must be defined.")
        }

        file { [ "/var/lib/ratbox-services/${name}",
                 "/var/lib/ratbox-services/${name}/etc",
                 "/var/lib/ratbox-services/${name}/etc/ratbox-services",
                 "/var/lib/ratbox-services/${name}/usr",
                 "/var/lib/ratbox-services/${name}/usr/share",
                 "/var/lib/ratbox-services/${name}/usr/share/ratbox-services",
                 "/var/lib/ratbox-services/${name}/usr/share/ratbox-services/help",
                 "/var/lib/ratbox-services/${name}/usr/share/ratbox-services/langs",
                 "/var/lib/ratbox-services/${name}/var",
                 "/var/lib/ratbox-services/${name}/var/log",
                 "/var/lib/ratbox-services/${name}/var/log/ratbox-services",
                 "/var/lib/ratbox-services/${name}/var/run",
                 "/var/lib/ratbox-services/${name}/var/run/ratbox-services", ]:
            ensure  => directory,
            owner   => "irc",
            group   => "irc",
            mode    => "0600",
            before  => Service["ratbox-services-${name}"],
            require => File["/var/lib/ratbox-services"],
        }

        File["/var/lib/ratbox-services/${name}/usr/share/ratbox-services/help"] {
            source  => "/usr/share/ratbox-services/help",
            recurse => true,
        }

        file { "/var/lib/ratbox-services/${name}/etc/ratbox-services/ratbox-services.conf":
            ensure  => present,
            mode    => "0600",
            owner   => "irc",
            group   => "irc",
            content => template("irc/ratbox-services.conf.erb"),
            before  => Service["ratbox-services-${name}"],
            notify  => Service["ratbox-services-${name}"],
            require => File["/var/lib/ratbox-services/${name}/etc/ratbox-services"],
        }

        file { "/var/lib/ratbox-services/${name}/etc/ratbox-services/ratbox-services.db":
            ensure  => present,
            mode    => "0600",
            owner   => "irc",
            group   => "irc",
            source  => "/etc/ratbox-services/ratbox-services.db",
            replace => false,
            before  => Service["ratbox-services-${name}"],
            require => File["/var/lib/ratbox-services/${name}/etc/ratbox-services"],
        }

        file { "/etc/init.d/ratbox-services-${name}":
            ensure  => present,
            mode    => "0755",
            owner   => "root",
            group   => "root",
            content => template("irc/ratbox-services.init.erb"),
            before  => Service["ratbox-services-${name}"],
            notify  => Exec["enable-ratbox-services-${name}"],
        }

        exec { "enable-ratbox-services-${name}":
            path        => "/bin:/usr/bin:/sbin:/usr/sbin",
            command     => "update-rc.d ratbox-services-${name} defaults",
            refreshonly => true,
            before      => Service["ratbox-services-${name}"],
        }

        service { "ratbox-services-${name}":
            ensure  => running,
            enable  => true,
            status  => "pgrep -u irc -f /var/lib/ratbox-services/${name}",
        }
    }

}
