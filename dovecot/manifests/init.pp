class dovecot::common {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            if versioncmp($::operatingsystemrelease, "6") < 0 {
                fail("Dovecot module requires ${::operatingsystem} 6")
            }
            package { "dovecot":
                ensure => installed,
            }
        }
        default: {
            fail("Dovecot module not supported in ${::operatingsystem}.")
        }
    }
}


# === Global variables
#
#   $dovecot_mail_domain:
#       Mail domain name.
#   $dovecot_ssl_csr:
#       Puppet source for the CSR file.
#   $dovecot_ssl_cert:
#       Puppet source for the X.509 certificate.
#   $dovecot_ssl_key:
#       Puppet source for the X.509 key.
#   $dovecot_ssl_ca:
#       Puppet source for the optional X.509 ca certificate.
#   $dovecot_mailbox_format:
#       Mailbox format to use in user's homedir ["mbox" | "mdbox"]
#   $dovecot_zlib:
#       Compress mailboxes with zlib ["yes" | "no"]
#
class dovecot::server inherits dovecot::common {

    if ! $dovecot_mailbox_format {
        $dovecot_mailbox_format = "mbox"
    }

    service { "dovecot":
        ensure => running,
        enable => true,
    }

    file { "/etc/dovecot/conf.d/98-puppet.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("dovecot/puppet.conf.erb"),
        notify  => Service["dovecot"],
        require => Package["dovecot"],
    }

    file { "/etc/dovecot/conf.d/99-local.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [
            "puppet:///files/dovecot/local.conf",
            "puppet:///modules/dovecot/empty",
        ],
        notify  => Service["dovecot"],
        require => Package["dovecot"],
    }


    if ! ($dovecot_ssl_key and $dovecot_ssl_cert) {
        fail("Must define \$dovecot_ssl_key and \$dovecot_ssl_cert")
    }

    include ssl

    file { "${ssl::private}/dovecot.key":
        ensure => present,
        source => $dovecot_ssl_key,
        mode   => "0600",
        owner  => "root",
        group  => "root",
        notify => Service["dovecot"],
    }

    file { "${ssl::certs}/dovecot.crt":
        ensure => present,
        source => $dovecot_ssl_cert,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        notify => Service["dovecot"],
    }

    if $dovecot_ssl_csr {
        file { "${ssl::private}/dovecot.csr":
            ensure => present,
            source => $dovecot_ssl_csr,
            mode   => "0640",
            owner  => "root",
            group  => "root",
            notify => Service["dovecot"],
        }
    }

    if $dovecot_ssl_ca {
        file { "${ssl::certs}/dovecot.ca.crt":
            ensure => present,
            source => $dovecot_ssl_ca,
            mode   => "0644",
            owner  => "root",
            group  => "root",
            notify => Service["dovecot"],
        }
    }

}


# Install custom config to Dovecot
#
# === Parameters
#
#   $name:
#       Config name.
#
#   $idx:
#       Config load order. Defaults to 99.
#
# === Sample usage
#
# dovecot::server::config { "passdb-pam": idx => 09 }
#
define dovecot::server::config($idx = 90) {

    include dovecot::server

    file { "${name}.conf":
        ensure  => present,
        path    => "/etc/dovecot/conf.d/${idx}-${name}.conf",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => [
            "puppet:///files/dovecot/${name}.conf",
            "puppet:///modules/dovecot/${name}.conf",
        ],
        notify  => Service["dovecot"],
        require => Package["dovecot"],
    }

}
