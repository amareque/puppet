require 'set'

Facter.add(:raid) do
    confine :kernel => :linux
    setcode do
        raid = Set.new
        mdstat = "/proc/mdstat"
        if File.exists?(mdstat)
            File.readlines(mdstat).each do |line|
                if line =~ /^md[0-9]+/
                    raid << "linux-md"
                    break
                end
            end
        end
        modules = "/proc/modules"
        if File.exists?(modules)
            File.readlines(modules).each do |line|
                case line
                when /^cciss\s/,/^hpsa\s/
                    raid << "smartarray"
                when /^megaraid_sas\s/
                    raid << "megaraid"
                end
            end
        end
        raid.sort.join(',')
    end
end
