
# Install Samba client tools
#
class samba::client {

    package { "samba-client":
        ensure => installed,
    }

}


# Add included samba configuration files
#
# This class is used internally by samba to override various configs.
#
class samba::server::configs {

    file { "/etc/samba/smb.conf.d/print.conf":
        ensure  => present,
        content => "load printers = no\nprintcap name = /dev/null\n",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => File["/etc/samba/smb.conf.d"],
        notify  => Service["smb"],
    }

    file { "/etc/samba/smb.conf.d/domain.conf":
        ensure  => present,
        content => "os level = 20\n",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => File["/etc/samba/smb.conf.d"],
        notify  => Service["smb"],
    }

    file { "/etc/samba/smb.conf.d/wins.conf":
        ensure  => present,
        content => $samba_wins ? {
            ""      => "name resolve order = lmhosts host bcast\n",
            default => "name resolve order = lmhosts host wins bcast\nwins server=${samba_wins}\n",
        },
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => File["/etc/samba/smb.conf.d"],
        notify  => Service["smb"],
    }

}


# Install Samba server
#
# === Parameters
#
#     $auth:
#         Server authentication type. Valid values are tdbsam, ldap and
#         domain. Default is tdbsam.
#     $description:
#         Server description. Defaults to "Samba Server Version %v".
#     $interfaces:
#         Array of intefaces samba should listen to. See "interfaces" option
#         from smb.conf manual page for syntax. Localhost interface is
#         added automatically. Defaults to all active interfaces.
#     $names:
#         Array of NetBIOS names that host will be advertised. Defaults to
#         [$::hostname].
#     $workgroup:
#         Workgroup or domain name.
#
# === Global variables
#
#     $samba_wins:
#         WINS server address
#
#     $ldap_server:
#         LDAP server URI's to use. Only used when $auth is set to ldap.
#     $ldap_basedn:
#         LDAP basedn. Only used when $auth is set to ldap.
#
#     $samba_join_user:
#         Username to use when joining to domain. Only used when $auth is
#         set to domain.
#     $samba_join_pass:
#         Password to use when joining to domain. Only used when $auth is
#         set to domain.
#
class samba::server($names=[$::hostname], $auth="tdbsam",
                    $workgroup="WORKGROUP",
                    $description="Samba Server Version %v",
                    $interfaces=undef) {

    include samba::server::configs

    package { "samba":
        ensure => installed,
    }

    file { "/etc/samba/smb.conf":
        ensure  => present,
        content => template("samba/smb.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["samba"],
        notify  => Service["smb"],
    }

    file { "/etc/samba/smb.conf.d":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        require => Package["samba"],
    }

    exec { "generate-samba-shares-conf":
        command     => "find /etc/samba/smb.conf.d/share-*.conf -exec echo 'include = {}' \\; > /etc/samba/smb.conf.d/shares.conf",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        notify      => Service["smb"],
    }
    file { "/etc/samba/smb.conf.d/shares.conf":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

    file { "/etc/samba/lmhosts":
        ensure => present,
        source => [ "puppet:///files/samba/lmhosts.${::homename}",
                    "puppet:///files/samba/lmhosts",
                    "puppet:///modules/samba/lmhosts", ],
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["samba"],
    }

    case $auth {
        "ldap": {
            exec { "smbpasswd -w":
                command     => "smbpasswd -w \"\${SECRET}\"",
                environment => "SECRET=${samba_ldap_pass}",
                path        => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
                unless      => "fgrep SECRETS/LDAP_BIND_PW /var/lib/samba/private/secrets.tdb",
                require     => File["/etc/samba/smb.conf"],
                notify      => Service["smb"],
            }
        }
        "domain": {
            exec { "net join":
               command => "net join -U ${samba_join_user}%\"\${SECRET}\"",
               environment => "SECRET=${samba_join_pass}",
               path        => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
               unless      => "net rpc testjoin -U ${samba_join_user}%\"\${SECRET}\"",
               require     => Service["smb"],
            }
        }
    }

    service { "smb":
        name   => $::operatingsystem ? {
            "openbsd" => "smbd",
            "ubuntu"  => "smbd",
            default   => "smb",
        },
        ensure => running,
        enable => true,
    }
    service { "nmb":
        name   => $::operatingsystem ? {
            "openbsd" => "nmbd",
            "ubuntu"  => "nmbd",
            default   => "nmb",
        },
        ensure    => running,
        enable    => true,
        subscribe => Service["smb"],
    }

}


# Add WINS server role to Samba server
#
class samba::server::wins inherits samba::server::configs {

    File["/etc/samba/smb.conf.d/wins.conf"] {
        content => "name resolve order = lmhosts host wins bcast\nwins support = true\ndns proxy = true\n",
    }

}


# Add Primary Domain Controller role to Samba server
#
class samba::server::pdc inherits samba::server::configs {

    file { "/srv/netlogon":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        seltype => "samba_share_t",
        before  => Service["smb"],
    }
    selinux::manage_fcontext { "/srv/netlogon(/.*)?":
        type   => "samba_share_t",
        before => File["/srv/netlogon"],
    }

    samba::server::share { "netlogon":
        path     => "/srv/netlogon",
        comment  => "Network Logon Service",
        readonly => true,
        options  => [ "locking = no", "guest ok = yes", ],
    }

    File["/etc/samba/smb.conf.d/domain.conf"] {
        content => template("samba/domain.conf-pdc.erb"),
    }

}


# Add Backup Domain Controller role to Samba server
#
class samba::server::bdc inherits samba::server::pdc {

    require samba::client

    file { "/usr/local/sbin/sync-netlogon":
        ensure => present,
        source => "puppet:///modules/samba/sync-netlogon",
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }
    cron { "sync-netlogon":
        command => "/usr/local/sbin/sync-netlogon",
        minute  => "52",
        user    => "root",
        require => File["/usr/local/sbin/sync-netlogon"],
    }

    File["/etc/samba/smb.conf.d/domain.conf"] {
        content => template("samba/domain.conf-bdc.erb"),
    }

}


# Add Home directory server role to Samba server
#
class samba::server::homes {

    if !defined(Selinux::Boolean["samba_enable_home_dirs"]) {
        selinux::boolean { "samba_enable_home_dirs":
            value  => "on",
            before => Service["smb"],
        }
    }

    samba::server::share { "homes":
        path    => "%H",
        comment => "Home Directories",
        options => [ "veto files = /.windows/", "browseable = no", ],
    }

}


# Add Profile server role to Samba server
#
class samba::server::profiles {

    if !defined(Selinux::Boolean["samba_enable_home_dirs"]) {
        selinux::boolean { "samba_enable_home_dirs":
            value  => "on",
            before => Service["smb"],
        }
    }

    selinux::boolean { "samba_create_home_dirs":
        value  => on,
        before => Service["smb"],
    }

    file { "/srv/profiles":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        seltype => "samba_share_t",
        before  => Service["smb"],
    }
    selinux::manage_fcontext { "/srv/profiles(/.*)?":
        type   => "samba_share_t",
        before => File["/srv/profiles"],
    }

    samba::server::share { "profiles":
        path    => "/srv/profiles",
        comment => "Roaming Profiles",
        options => [
          "wide links = yes",
          "profile acls = yes",
          "root preexec = sh -c 'umask 022 ; ( [ -h /srv/profiles/%U ] || ln -s %H/.windows/profile /srv/profiles/%U ) ; ( [ -h /srv/profiles/%U.V2 ] || ln -s %H/.windows/vista /srv/profiles/%U.V2 )'",
          "preexec = sh -c 'umask 077; mkdir -p %H/.windows/profile %H/.windows/vista'",
        ],
    }

}


# Add new share to Samba server
#
# === Parameters
#
#     $name:
#         Share name
#     $path:
#         Directory to share
#     $comment:
#         Share description. Defaults to $name.
#     $readonly:
#         Set to true to make share read only.
#     $options:
#         Array of extra options to add for share.
#
# === Sample usage
#
# samba::server::share { "r-sysadm":
#     comment => "Role: sysadm",
#     path    => "/roles/sysadm",
#     options => [ "hide files = /desktop.ini/Desktop.ini/" ],
# }
#
define samba::server::share($path, $comment=undef, $readonly=undef,
                            $options=[]) {

    file { "/etc/samba/smb.conf.d/share-${name}.conf":
        ensure  => present,
        content => template("samba/share.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => File["/etc/samba/smb.conf.d"],
        notify  => Exec["generate-samba-shares-conf"]
    }

}

