
class mutt {

    package { "mutt":
        ensure => installed,
    }

    if ! $mail_server {
        $mail_server = "mail.${domain}"
    }
    if ! $mail_domain {
        $mail_domain = $domain
    }

    case $::operatingsystem {
        openbsd: { $muttconfdir = "/etc/mutt/Muttrc" }
        default: { $muttconfdir = "/etc" }
    }

    exec { "add-local-mutt-config":
        command => "echo 'source ${muttconfdir}/Muttrc.local' >> ${muttconfdir}/Muttrc",
        path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        user    => root,
        unless  => "fgrep 'source ${muttconfdir}/Muttrc.local' ${muttconfdir}/Muttrc",
        require => Package["mutt"],
    }

    file { "Muttrc.local":
        ensure  => present,
        name    => "${muttconfdir}/Muttrc.local",
        content => template("mutt/Muttrc.local.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            openbsd => "wheel",
            default => "root",
        },
        require => Exec["add-local-mutt-config"],
    }

}
