
# Install spamassassin common components
#
class spamassassin::common {

    package { "spamassassin":
        name   => $::operatingsystem ? {
            "openbsd" => "p5-Mail-SpamAssassin",
            default   => "spamassassin",
        },
        ensure => present,
    }

    file { "/etc/mail/spamassassin/local.cf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => [
            "puppet:///files/mail/spamassassin.cf.${::homename}",
            "puppet:///files/mail/spamassassin.cf",
            "puppet:///modules/spamassassin/local.cf",
        ],
        notify  => Service["spamassassin"],
        require => Package["spamassassin"],
    }

    service { "spamassassin":
        ensure    => running,
        enable    => true,
        binary    => $::operatingsystem ? {
            "openbsd" => "/usr/local/bin/spamd -d",
            default   => undef,
        },
        start     => $::operatingsystem ? {
            "openbsd" => "/usr/local/bin/spamd -d -c -H -m 5",
            default   => undef,
        },
        hasstatus => true,
        require   => Package["spamassassin"],
    }

}


# Install spamassassin using procmail
#
class spamassassin::procmail {

    include spamassassin::common
    include procmail

    $spamc = $::operatingsystem ? {
        "openbsd" => "/usr/local/bin/spamc",
        default   => "/usr/bin/spamc",
    }

    procmail::rc { "10-spamassassin.rc":
        content => ":0fw\n| ${spamc}\n",
    }

}


# Install spamassassin milter
#
# === Parameters
#
#   $options:
#       Extra options for spamass-milter.
#
class spamassassin::milter($options="") {

    include spamassassin::common

    $sendmail_milter = [
        # filter name
        'spamass',
        # filter options
        'S=unix:/var/run/spamass-milter/spamass-milter.sock, F=, T=C:15m;S:4m;R:4m;E:10m',
    ]

    package { "spamass-milter":
        ensure => installed,
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            augeas { "set-spamass-milter-sysconfig":
                changes => "set EXTRA_FLAGS '\"${options}\"'",
                incl    => "/etc/sysconfig/spamass-milter",
                lens    => "Shellvars.lns",
                require => Package["spamass-milter"],
                notify  => Service["spamass-milter"],
            }
        }
        default: {
            fail("spamassassin::milter not supported on ${::operatingsystem}")
        }
    }

    service { "spamass-milter":
        ensure => running,
        enable => true,
    }

}
