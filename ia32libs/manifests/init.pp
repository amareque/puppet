# Install 32-bit compatibility libraries for 64-bit systems.
#
class ia32libs {

    case $::architecture {
        "amd64","x86_64": {
            case $::operatingsystem {
                "ubuntu": {
                    package { "ia32-libs":
                        ensure => installed,
                    }
                }
            }
        }
    }

}
