
# Install Clam AntiVirus
#
# === Parameters
#
#     $update:
#         Set to false to disable automatic virus database updates.
#
class clamav($update="true") {

    package { "clamav":
        ensure => installed,
    }

    if $update == false {
        file { "/etc/cron.daily/freshclam":
            ensure => absent,
        }
    }

}


# Scan directories periodically
#
# === Parameters:
#
#     $name:
#         Directory path to scan.
#     $hour:
#         At what hour should scanning occur. Defaults to 4am.
#     $weekday:
#         At what day should sacnning occur. For daily scanning you
#         can use "*". Defaults to Sunday.
#     $exclude:
#         Directories matching to this regex will be excluded from
#         scanning. Default is to scan everything.
#
# === Sample usage:
#
# clamav::scan { "/export/roles":
#     exclude => "/export/roles/[a-z]*/library/archive",
# }
#
define clamav::scan($hour="04", $weekday="Sunday", $exclude=undef) {

    require clamav

    if $exclude {
        $exclude_opts = "--exclude-dir='${exclude}'"
    }

    cron { "virusscan-${name}":
        command => "clamscan -r --infected --no-summary ${name} ${exclude_opts}",
        user    => "root",
        hour    => $hour,
        minute  => "00",
        weekday => $weekday,
    }

}
