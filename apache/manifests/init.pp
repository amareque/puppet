# Install Apache, www logrotate script and cron job.
#
class apache::common {

    case $::operatingsystem {
        debian,ubuntu: {
            if ! $httpd_user {
                $user = "www-data"
            } else {
                $user = $httpd_user
            }
            if ! $httpd_group {
                $group = "www-data"
            } else {
                $group = $httpd_group
            }
        }
    }

    if $apache_datadir {
        file { $apache_datadir:
            ensure  => directory,
            mode    => "0755",
            owner   => "root",
            group   => "root",
            seltype => "httpd_sys_content_t",
        }
        selinux::manage_fcontext { "${apache_datadir}(/.*)?":
            type   => "httpd_sys_content_t",
            before => File[$apache_datadir],
        }
        selinux::manage_fcontext { "${apache_datadir}/log(/.*)?":
            type    => "httpd_log_t",
            before  => File["/srv/www/log"],
            require => Selinux::Manage_fcontext["${apache_datadir}(/.*)?"],
        }
        file { "/srv/www":
            ensure  => link,
            target  => $apache_datadir,
            require => File[$apache_datadir],
        }
    } else {
        file { "/srv/www":
            ensure => directory,
            mode   => "0755",
            owner  => root,
            group  => root,
        }
        selinux::manage_fcontext { "/srv/www/log(/.*)?":
            type   => "httpd_log_t",
            before => File["/srv/www/log"],
        }
    }

    file { "/srv/www/log":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        seltype => "httpd_log_t",
        require => File["/srv/www"],
    }

    package { "httpd":
        name   => $::operatingsystem ? {
            debian  => "apache2",
            ubuntu  => "apache2",
            default => "httpd",
        },
        ensure => installed,
    }

    file { "/usr/local/sbin/www-logrotate.sh":
        ensure  => present,
        source  => "puppet:///modules/apache/www-logrotate.sh",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        seltype => "httpd_rotatelogs_exec_t",
    }
    if $::operatingsystem == "Fedora" and $::operatingsystemrelease > 17 {
        selinux::manage_fcontext { "/usr/sbin/www-logrotate.sh":
            type   => "httpd_rotatelogs_exec_t",
            before => File["/usr/local/sbin/www-logrotate.sh"],
        }
    } else {
        selinux::manage_fcontext { "/usr/local/sbin/www-logrotate.sh":
            type   => "httpd_rotatelogs_exec_t",
            before => File["/usr/local/sbin/www-logrotate.sh"],
        }
    }

    cron { "www-logrotate":
        ensure  => present,
        command => "/usr/local/sbin/www-logrotate.sh",
        user    => "root",
        hour    => "0",
        minute  => "0",
        weekday => "1",
        require => File["/usr/local/sbin/www-logrotate.sh"],
    }

}


# Configure HTTP server.
#
# === Global variables
#
#   $httpd_user:
#       User httpd runs as. Defaults to apache.
#   $httpd_group:
#       Group httpd runs as. Defaults to apache.
#
class apache::server inherits apache::common {

    case $::operatingsystem {
        "debian","ubuntu": {
            include apache::debian::server
        }
        "centos","redhat","fedora": {
            if ! $httpd_user {
                $user = "apache"
            } else {
                $user = $httpd_user
            }
            if ! $httpd_group {
                $group = "apache"
            } else {
                $group = $httpd_group
            }
            include apache::redhat::server
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Configure HTTP virtual host.
#
# === Parameters
#
#   $name:
#       FQDN of virtual host.
#   $aliases:
#       Optional ServerAlias for this virtual host.
#   $root:
#       Path to document root. Defaults to /srv/www/http/$homename
#   $redirect:
#       Add redirect to given URL.
#   $proxy:
#       Proxy site to given URL.
#
# === Sample usage
#
# apache::site { "default":
#     redirect => "http://www.example.com/",
# }
# apache::site { "www.example.com":
#     root   => "/roles/prteam/public/public_access",
# }
# apache::site { "www2.example.com":
#     proxy => "http://www.example.com",
# }
#
define apache::site($aliases="", $root="", $redirect="", $proxy="") {

    if $redirect and $proxy {
        fail("cannot define both \$redirect and \$proxy for apache::site (${name})")
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::site { $name:
                aliases  => $aliases,
                root     => $root,
                redirect => $redirect,
                proxy    => $proxy,
            }
        }
        "centos","redhat","fedora": {
            apache::redhat::site { $name:
                aliases  => $aliases,
                root     => $root,
                redirect => $redirect,
                proxy    => $proxy,
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

    if !$redirect {
        $site_fqdn = $name ? {
            "default" => $::homename,
            default   => $name,
        }
        apache::webalizer::site { "http/${site_fqdn}":
            site_proto => "http",
            site_fqdn  => $site_fqdn,
        }
    }

}


# Configure HTTPS server.
#
# === Global variables
#
#   $httpsd_user:
#       User httpsd runs as. Defaults to httpsd.
#   $httpsd_group:
#       Group httpsd runs as. Defaults to httpsd.
#
class apache::sslserver inherits apache::common {

    case $::operatingsystem {
        "debian","ubuntu": {
            include apache::debian::sslserver
        }
        "centos","redhat","fedora": {
            if ! $httpsd_user {
                $user = "httpsd"
            } else {
                $user = $httpsd_user
            }
            if ! $httpsd_group {
                $group = "httpsd"
            } else {
                $group = $httpsd_group
            }
            include apache::redhat::sslserver
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Enable SSL on port 443.
#
class apache::sslserver::listen {

    apache::configfile { "ssl.conf":
        content => template("apache/ssl.conf.erb"),
        http    => false,
        require => Class["apache::sslserver"],
    }

}


# Configure HTTPS virtual host.
#
# === Parameters
#
#   $name:
#       FQDN of virtual host.
#   $first:
#       Bool for whether this is the first (default) vhost
#       when using NameVirtualHost. Defaults to false.
#   $ipaddr:
#       IP address of virtual host. Defaults to _default_.
#   $root:
#       Path to document root. Defaults to /srv/www/https/$homename
#   $ssl_cert:
#       Path to SSL certificate. Defaults to puppet client certificate.
#   $ssl_key:
#       Path to SSL private key. Defaults to puppet client certificate.
#   $ssl_chain:
#       Path to SSL certificate chain. Defaults to none.
#
# === Sample usage
#
# apache::site { "www.example.com":
#     root     => "/roles/prteam/public/secure_access",
#     ssl_cert => "puppet:///path/to/www.example.com.crt",
#     ssl_key  => "puppet:///path/to/www.example.com.key",
# }
#
define apache::sslsite($first=false, $ipaddr="_default_", $root="", $ssl_cert="", $ssl_key="", $ssl_chain="") {

    include apache::sslserver::listen

    case $::operatingsystem {
        "debian","ubuntu": {
            $apache_ssldir = "/etc/ssl"
            apache::debian::sslsite { $name:
                first     => $first,
                ipaddr    => $ipaddr,
                root      => $root,
                ssl_cert  => $ssl_cert,
                ssl_key   => $ssl_key,
                ssl_chain => $ssl_chain,
                require   => Class["apache::sslserver::listen"],
            }
        }
        "centos","redhat","fedora": {
            $apache_ssldir = "/etc/pki/tls"
            apache::redhat::sslsite { $name:
                first     => $first,
                ipaddr    => $ipaddr,
                root      => $root,
                ssl_cert  => $ssl_cert,
                ssl_key   => $ssl_key,
                ssl_chain => $ssl_chain,
                require   => Class["apache::sslserver::listen"],
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

    $site_fqdn = $name ? {
        "default" => $::homename,
        default   => $name,
    }
    apache::webalizer::site { "https/${site_fqdn}":
        site_proto => "https",
        site_fqdn  => $site_fqdn,
    }

}


# Install extra configuration file.
#
# === Parameters
#
#   $name:
#       Config file name.
#   $source:
#       Config file source. Defaults to /etc/httpd/conf.d/$name
#       if neither $source nor $content is defined.
#   $content:
#       Config file content. See also $source.
#   $http:
#       Set to false to disable config on http server.
#   $https:
#       Set to false to disable config on https server.
#
# === Sample usage
#
#apache::configfile { "auth_kerb.conf":
#    content => template("apache/auth_kerb.conf.erb"),
#    require => Package["mod_auth_kerb"],
#    http    => false,
#}
#
define apache::configfile($source="", $content="", $http=true, $https=true) {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::configfile { $name:
                source  => $source,
                content => $content,
                http    => $http,
                https   => $https,
            }
        }
        "centos","redhat","fedora": {
            apache::redhat::configfile { $name:
                source  => $source,
                content => $content,
                http    => $http,
                https   => $https,
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Configure forward proxy.
#
# === Parameters
#
#   $port:
#       Port to listen. Defaults to "8080".
#   $allow:
#       Array of addresses or networks allowed to connect.
#       Defaults to ["127.0.0.1"].
#   $cache:
#       Enable disk cache. Defaults to false.
#   $cachesize:
#       Maximum size of the cache. Defaults to "1024M".
#   $cachecleantime:
#       Cache cleaning interval in minutes.
#       Defaults to "60".
#
class apache::proxy($port="8080",
                    $allow=["127.0.0.1"],
                    $cache=false,
                    $cachesize="1024M",
                    $cachecleantime="60") {

    include apache::mod::proxy

    if $cache == true {
        case $::operatingsystem {
            "centos","redhat","fedora": {
                $cachepath = "/var/cache/mod_proxy"

                augeas { "set-htcacheclean-sysconfig":
                    changes => [
                        "set INTERVAL ${cachecleantime}",
                        "set CACHE_ROOT ${cachepath}",
                        "set CACHE_LIMIT ${cachesize}",
                    ],
                    incl    => "/etc/sysconfig/htcacheclean",
                    lens    => "Shellvars.lns",
                    require => Package["httpd"],
                    notify  => Service["htcacheclean"],
                }

                service { "htcacheclean":
                    ensure => running,
                    enable => true,
                }
            }
            default: {
                fail("Caching proxy not supported in ${::operatingsystem}")
            }
        }
    }

    apache::configfile { "proxy.conf":
        https   => false,
        content => template("apache/proxy.conf.erb"),
    }

}


# Install mod_auth_kerb.
#
class apache::mod::auth_kerb {

    package { "mod_auth_kerb":
        ensure  => installed,
        require => Package["httpd"],
    }

    apache::configfile { "auth_kerb.conf":
        content => template("apache/auth_kerb.conf.erb"),
        require => Package["mod_auth_kerb"],
        http    => false,
    }

}


# Enable mod_auth_openid.
#
class apache::mod::auth_openid {

    apache::configfile { "auth_openid.conf":
        content => "LoadModule authopenid_module modules/mod_auth_openid.so\n",
        http    => false,
    }

}


# Install mod_authnz_ldap.
#
class apache::mod::authnz_ldap {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "authnz_ldap": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}

# Install mod_include
#
class apache::mod::include {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "include": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}

# Install mod_fcgid.
#
# === Parameters
#
#   $http:
#       Enable for http. Defaults to true.
#   $https:
#       Enable for https. Defaults to true.
#
class apache::mod::fcgid($http=true, $https=true) {

    package { "mod_fcgid":
        name    => $::operatingsystem ? {
            debian  => "libapache2-mod-fcgid",
            ubuntu  => "libapache2-mod-fcgid",
            default => "mod_fcgid",
        },
        ensure  => installed,
        require => Package["httpd"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "fcgid":
                require => Package ["libapache2-mod-fcgid"],
            }
        }
        "centos","redhat","fedora": {
            if $http == true and defined(Class["apache::server"]) {
                file { "/var/run/mod_fcgid":
                    ensure  => directory,
                    mode    => "0755",
                    owner   => $apache::server::user,
                    group   => $apache::server::group,
                    before  => Service["httpd"],
                }
                apache::configfile { "fcgid_http.conf":
                    source  => "puppet:///modules/apache/fcgid_http.conf",
                    https   => false,
                }
            }
            if $https == true and defined(Class["apache::sslserver"]) {
                file { "/var/run/mod_fcgid_httpsd":
                    ensure  => directory,
                    mode    => "0755",
                    owner   => $apache::sslserver::user,
                    group   => $apache::sslserver::group,
                    before  => Service["httpsd"],
                }
                apache::configfile { "fcgid_https.conf":
                    source  => "puppet:///modules/apache/fcgid_https.conf",
                    http    => false,
                }
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_headers.
#
class apache::mod::headers {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "headers": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_ldap.
#
class apache::mod::ldap {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "ldap": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_passenger.
#
# === Parameters
#
#   $http:
#       Enable for http. Defaults to true.
#   $https:
#       Enable for https. Defaults to true.
#
class apache::mod::passenger($http=true, $https=true) {

    package { "mod_passenger":
        name    => $::operatingsystem ? {
            debian  => "libapache2-mod-passenger",
            ubuntu  => "libapache2-mod-passenger",
            default => "mod_passenger",
        },
        ensure  => installed,
        require => Package["httpd"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "passenger":
                require => Package ["libapache2-mod-passenger"],
            }
        }
        "centos","redhat","fedora": {
            apache::configfile { "passenger.conf":
                http    => $http,
                https   => $https,
                require => Package["mod_passenger"],
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

    file { [ "/var/lib/passenger", "/var/run/passenger", ]:
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }

}


# Install mod_perl.
#
# === Parameters
#
#   $http:
#       Enable for http. Defaults to true.
#   $https:
#       Enable for https. Defaults to true.
#
class apache::mod::perl($http=true, $https=true) {

    package { "mod_perl":
        name    => $::operatingsystem ? {
            debian  => "libapache2-mod-perl2",
            ubuntu  => "libapache2-mod-perl2",
            default => "mod_perl",
        },
        ensure  => installed,
        require => Package["httpd"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "perl2":
                require => Package ["libapache2-mod-perl2"],
            }
        }
        "centos","redhat","fedora": {
            apache::configfile { "perl.conf":
                http    => $http,
                https   => $https,
                require => Package["mod_perl"],
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install PHP.
#
# === Parameters
#
#   $http:
#       Enable for http. Defaults to true.
#   $https:
#       Enable for https. Defaults to true.
#
class apache::mod::php($http=true, $https=true) {

    package { "php":
        name    => $::operatingsystem ? {
            debian  => "libapache2-mod-php5",
            ubuntu  => "libapache2-mod-php5",
            default => "php",
        },
        ensure  => installed,
        require => Package["httpd"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "php5":
                require => Package ["libapache2-mod-php5"],
            }
        }
        "centos","redhat","fedora": {
            apache::configfile { "php.conf":
                http    => $http,
                https   => $https,
                require => Package["php"],
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_proxy
#
class apache::mod::proxy {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "proxy": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_proxy_http
#
class apache::mod::proxy_http {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "proxy_http": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_proxy_balancer
#
class apache::mod::proxy_balancer {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "proxy_balancer": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_python.
#
# === Parameters
#
#   $http:
#       Enable for http. Defaults to true.
#   $https:
#       Enable for https. Defaults to true.
#
class apache::mod::python($http=true, $https=true) {

    package { "mod_python":
        name    => $::operatingsystem ? {
            debian  => "libapache2-mod-python",
            ubuntu  => "libapache2-mod-python",
            default => "mod_python",
        },
        ensure  => installed,
        require => Package["httpd"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "python":
                require => Package ["libapache2-mod-python"],
            }
        }
        "centos","redhat","fedora": {
            apache::configfile { "python.conf":
                http    => $http,
                https   => $https,
                require => Package["mod_python"],
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_rewrite.
#
class apache::mod::rewrite {

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "rewrite": }
        }
        "centos","redhat","fedora": { }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install mod_wsgi.
#
# === Parameters
#
#   $http:
#       Enable for http. Defaults to true.
#   $https:
#       Enable for https. Defaults to true.
#   $embedded:
#       Allow running scripts in embedded mode. Defaults to true.
#
class apache::mod::wsgi($http=true, $https=true, $embedded=true) {

    package { "mod_wsgi":
        name    => $::operatingsystem ? {
            debian  => "libapache2-mod-wsgi",
            ubuntu  => "libapache2-mod-wsgi",
            default => "mod_wsgi",
        },
        ensure  => installed,
        require => Package["httpd"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            apache::debian::a2enmod { "wsgi":
                require => Package ["libapache2-mod-wsgi"],
            }
        }
        "centos","redhat","fedora": {
            file { "/var/run/mod_wsgi":
                ensure => directory,
                mode   => "0755",
                owner  => "root",
                group  => "root",
                before => Apache::Configfile["wsgi.conf"],
            }
            apache::configfile { "wsgi.conf":
                http    => $http,
                https   => $https,
                content => template("apache/wsgi.conf.erb"),
                require => Package["mod_wsgi"],
            }
        }
        default: {
            fail("Apache module not supported in ${::operatingsystem}.")
        }
    }

}


# Install and configure webalizer.
#
class apache::webalizer {

    include user::system
    realize(User["webalizer"], Group["webalizer"])

    package { "webalizer":
        ensure  => installed,
        require => User["webalizer"],
    }

    file { "/srv/www/webalizer":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File["/srv/www"],
    }

    file { [ "/srv/www/webalizer/history",
             "/srv/www/webalizer/history/http",
             "/srv/www/webalizer/history/https",
             "/srv/www/webalizer/html",
             "/srv/www/webalizer/html/http",
             "/srv/www/webalizer/html/https", ]:
        ensure  => directory,
        mode    => "0755",
        owner   => "webalizer",
        group   => "root",
        require => [ File["/srv/www/webalizer"], User["webalizer"] ],
    }

    file { [ "/etc/webalizer",
             "/etc/webalizer/http",
             "/etc/webalizer/https", ]:
        ensure  => directory,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["webalizer"],
    }

    File["/etc/webalizer/http", "/etc/webalizer/https"] {
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    file { "/etc/cron.daily/00webalizer":
        name    => $::operatingsystem ? {
            "debian" => "/etc/cron.daily/webalizer",
            "ubuntu" => "/etc/cron.daily/webalizer",
             default => "/etc/cron.daily/00webalizer",
        },
        ensure  => absent,
        require => Package["webalizer"],
    }

    file { "/usr/local/sbin/www-webalizer.sh":
        ensure  => present,
        source  => "puppet:///modules/apache/www-webalizer.sh",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => Package["webalizer"],
    }

    cron { "www-webalizer":
        ensure      => present,
        environment => "MAILTO=root",
        command     => "/usr/local/sbin/www-webalizer.sh",
        user        => "webalizer",
        hour        => 23,
        minute      => 55,
        require     => File["/usr/local/sbin/www-webalizer.sh"],
    }

    File <| tag == "webalizer" |>

}


define apache::webalizer::site($site_proto, $site_fqdn) {

    @file { "/etc/webalizer/${name}.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("apache/webalizer.conf.erb"),
        require => File["/etc/webalizer/${site_proto}"],
        tag     => "webalizer",
    }

    @file { [ "/srv/www/webalizer/history/${name}",
              "/srv/www/webalizer/html/${name}", ]:
        ensure  => directory,
        mode    => "0755",
        owner   => "webalizer",
        group   => "root",
        require => [
            File["/srv/www/webalizer/history/${site_proto}"],
            File["/srv/www/webalizer/html/${site_proto}"],
        ],
        tag     => "webalizer",
    }

}
