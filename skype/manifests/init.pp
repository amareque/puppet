
# Install Skype
#
class skype {

    case $::operatingsystem {
        "fedora": { include yum::repo::skype }
        "ubuntu": { include apt::repo::partner }
        default: { fail("Skype module not supported in ${::operatingsystem}") }
    }

    package { "skype":
        ensure => installed,
    }

}
