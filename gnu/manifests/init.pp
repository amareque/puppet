# Install GCC.
#
# On Linux hosts this will also install headers required for compiling
# kernel modules.
#
class gnu::gcc {

    package { "gcc":
        ensure => installed,
    }

    if $::kernel == Linux {
        package { "kernel-headers":
            name   => $::operatingsystem ? {
                "debian" => "linux-libc-dev",
                "ubuntu" => "linux-libc-dev",
                default  => [ "kernel-headers", "kernel-devel", ],
            },
            ensure => installed,
        }
    }

}


# Install GNU make.
#
class gnu::make {

    package { "make":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "gmake",
            default   => "make",
        },
    }

}


# Install GNU tar.
#
class gnu::tar {

    package { "tar":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "gtar",
            default   => "tar",
        },
    }

}
