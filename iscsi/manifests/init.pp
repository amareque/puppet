
# Install iSCSI target prequisites.
#
class iscsi::server {

    package { "scsi-target-utils":
        ensure => installed,
    }

    service { "tgtd":
        ensure  => running,
        enable  => true,
        require => Package["scsi-target-utils"],
    }

    file { "/etc/tgt/targets.conf":
        ensure  => present,
        source  => [ "puppet:///files/iscsi/targets.conf.${::homename}",
                     "puppet:///files/iscsi/targets.conf",
                     "puppet:///modules/iscsi/targets.conf", ],
        mode    => "0600",
        owner   => root,
        group   => root,
        require => Package["scsi-target-utils"],
    }

    file { "/etc/tgt/target.d":
        ensure  => directory,
        mode    => "0700",
        owner   => root,
        group   => root,
        require => Package["scsi-target-utils"],
    }

    exec { "iscsi-refresh":
        command => "tgt-admin -e",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        onlyif  => "tgt-admin -e -p | egrep '^tgtadm '",
        require => Service["tgtd"],
    }

}


# Create iSCSI target.
#
# === Parameters
#
#   $name:
#       Path to block device that will be used as target.
#
#   $tid:
#       Target ID.
#
#   $initiator:
#       List of hosts that are allowed to connect to this target.
#       Defaults to ALL.
#
#   $ensure:
#       If set to present target will be created and if set to absent
#       target will be removed.
#
# === Sample usage
#
# iscsi::target { "/dev/vg0/iscsi":
#     ensure    => present,
#     tid       => 1,
#     initiator => "192.168.1.2",
# }
define iscsi::target($tid, $initiator = "ALL", $ensure = "present") {

    include iscsi::server

    $iqn = sprintf("iqn.2005-08.tgt:%s%s", $hostname, regsubst($name, "/", ".", "G"))

    case $ensure {
        "present": {
            file { "/etc/tgt/target.d/${tid}.conf":
                ensure  => present,
                content => template("iscsi/tid.conf.erb"),
                mode    => "0600",
                owner   => root,
                group   => root,
                require => File["/etc/tgt/target.d"],
                before  => Exec["iscsi-refresh"],
            }

        }
        "absent": {
            file { "/etc/tgt/target.d/${tid}.conf":
                ensure => absent,
                before => Exec["iscsi-refresh"],
            }
        }
    }

}


# Install iSCSI initiator software.
#
class iscsi::initiator {

    package { "iscsi-initiator-utils":
        ensure => installed,
    }

    service { "iscsid":
        ensure  => running,
        enable  => true,
        require => Package["iscsi-initiator-utils"],
    }

}

# Connect to iSCSI target.
#
# === Parameters
#
#   $name:
#       IQN of iSCSI target to connect to.
#
#   $portal:
#       Address of iSCSI target server.
#
# === Sample usage
#
# iscsi::connect { "iqn.2005-08.tgt:vm0001.dev.vg0.iscsi":
#     portal => "192.168.100.1",
# }
define iscsi::connect($portal) {

    include iscsi::initiator

    exec { "iscsi-connect-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "iscsiadm --mode discovery --type sendtargets --portal ${portal} && iscsiadm --mode node --targetname ${name} --portal ${portal} --login && sleep 5",
        unless  => "iscsiadm --mode session | egrep '${portal}:[0-9]*,[0-9]* ${name}'\$",
        require => Service["iscsid"],
    }

}
